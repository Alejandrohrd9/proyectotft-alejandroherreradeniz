<!DOCTYPE html>
<html>

    <head>
        <title>Inicio</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php include 'header-link.php'; ?>
        <!-- Hojas de estilo externas -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    </head>

    <body>
        <?php
        include 'db_functions.php';
        ?>
        <div class='container' >
            <?php include 'header.php' ?>
            <div class="main-wrapper">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="img/redi/teror.jpg" alt="Teror">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/redi/roque.jpg" alt="Roque Nublo">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/redi/dunas.jpg" alt="Dunas de Maspalomas">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="img/redi/pico.jpg" alt="Pico de las nieves">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="main-wrapper">
                <div class="description-section">
                    <h1 id="name-page">Camina y Descubre Gran Canaria</h1>
                </div>
            </div>
            <div class="main-wrapper" style="height: 100px">
                <div class="feature"><i class="fas fa-mountain fa-lg"></i>
                    <p class="feature-text">Explora las rutas de la isla de Gran Canaria.</p>
                </div>
                <div class="feature"><i class="fas fa-map-marked-alt fa-lg"></i>
                    <p class="feature-text">Selecciona el sendero que quieres conocer.</p>
                </div>
                <div class="feature"><i class="fas fa-info fa-lg"></i>
                    <p class="feature-text">Consulta información de los senderos.</p>
                </div>
                <div class="feature"><i class="fas fa-walking fa-lg"></i>
                    <p class="feature-text">Camina por los senderos sin moverte de casa.</p>
                </div>
            </div>
            <div class="main-wrapper photo">
                <div id="img"></div>
            </div>
            <?php include 'footer.php'; ?>
        </div>
    </body>

</html>