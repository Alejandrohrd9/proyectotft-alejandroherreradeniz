var table = [];
$.ajax({
    url: './raster/raster.csv',
    dataType: 'text'
}).done(loadData);


function loadData(data){
    var rows = data.split(/\r?\n|\r/);
    for (var i = 0; i < rows.length-1; i++) {
        var rowsCell = rows[i].split(',');
        table.push(rowsCell);
    }
}


function getLabelPoints(nearness){
    var ajax = new XMLHttpRequest();
    ajax.open("POST","query/placeLabelsOne.php",false);
    ajax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    ajax.send("nearness="+nearness);
    return ajax.responseText;
}

function addLabelsPoint(text){
    var json=plainTextToJson(text);
    for (var i = 0; i < json.features.length; i++) {
        var featureCoordinates = json.features[i].geometry.coordinates;
    }
    var source = new ol.source.Vector({
        features: (new ol.format.GeoJSON()).readFeatures(json)
    });
    return new ol.layer.Vector({
        source: source,
        altitudeMode: 'clampToGround',
        style: function(feature){ 
            return new ol.style.Style({
                fill: new ol.style.Fill({
                color: 'rgba(255,255,255,0.4)',
            }),
            stroke: new ol.style.Stroke({
                color: '#3399CC',
                width: 1.25
            }),
            text: new ol.style.Text({
                font: '15px Calibri,sans-serif',
                fill: new ol.style.Fill({ color: '#ffff66' }),
                stroke: new ol.style.Stroke({
                    color: '#696969', width: 1
                }),
                text: feature.get('name'),
            })
            });
        },
        name: 'label2'
    });
    
}

function addLabelsTown(text){
    var json=plainTextToJson(text);
    for (var i = 0; i < json.features.length; i++) {
        var featureCoordinates = json.features[i].geometry.coordinates;
        featureCoordinates[0][2] = 550;
        if(json.features[i].properties.name === "ARUCAS") featureCoordinates[0][2] = 400;
        //console.log(json.features[i].geometry.coordinates);
    }
    console.log(json);
    var source = new ol.source.Vector({
        features: (new ol.format.GeoJSON()).readFeatures(json)
    });
    
    return new ol.layer.Vector({
        source: source,
        //altitudeMode: 'clampToGround',
        //altitudeMode: 'relativeToGround',
        style: function(feature){ 
            return new ol.style.Style({
                fill: new ol.style.Fill({
                color: 'rgba(255,255,255,0.4)',
                
            }),
            stroke: new ol.style.Stroke({
                color: '#3399CC',
                width: 1.25
            }),
            text: new ol.style.Text({
                font: '18px Calibri,sans-serif',
                fill: new ol.style.Fill({ color: '#FAFAFA' }),
                stroke: new ol.style.Stroke({
                    color: '#696969', width: 1
                }),
                text: feature.get('name')
            })
            });
        },
        name: 'label1'
    });
    
}

var labelOne, labelTwo, labelThree, labelFour, labelFive, labelSix, labelSeven;
function zoomLabelsMap(){
    labelTwo = addLabelsPoint(getLabelPoints(2));;
    map.addLayer(labelTwo);
    labelTwo.setVisible(false);
    
    labelThree = addLabelsPoint(getLabelPoints(3));
    map.addLayer(labelThree);
    labelThree.setVisible(false);
    
    labelFour = addLabelsPoint(getLabelPoints(4));;
    map.addLayer(labelFour);
    labelFour.setVisible(false);
    
    labelFive = addLabelsPoint(getLabelPoints(5));
    map.addLayer(labelFive);
    labelFive.setVisible(false);
    
    labelSix = addLabelsPoint(getLabelPoints(6));
    map.addLayer(labelSix);
    labelSix.setVisible(false);
    
    labelSeven = addLabelsPoint(getLabelPoints(7));
    map.addLayer(labelSeven);
    labelSeven.setVisible(false);
    
    if(map.getView().getZoom() >=10){
        labelOne = addLabelsTown(getLabelPoints(1));
        map.addLayer(labelOne);
        
        
    }
    let isMapResolutionChanged;
    map.getView().on('change:resolution', () => {
        isMapResolutionChanged = true;
    });

    map.on('moveend', () => {
        if (isMapResolutionChanged) {
            if(map.getView().getZoom() >=13 && labelTwo !== undefined){
                labelTwo.setVisible(true);
            }
            if(map.getView().getZoom() < 13 && labelTwo !== undefined){
                labelTwo.setVisible(false);
            }
            if(map.getView().getZoom() >=14.5 && labelThree !== undefined){
                labelThree.setVisible(true);
                labelFour.setVisible(true);
            }
            if(map.getView().getZoom() < 14.5 && labelThree !== undefined){
                labelThree.setVisible(false);
                labelFour.setVisible(false);
            }
            if(map.getView().getZoom() >=16 && labelFive !== undefined){
                labelFive.setVisible(true);
            }
            if(map.getView().getZoom() <16 && labelFive !== undefined){
                labelFive.setVisible(false);
            }
             if(map.getView().getZoom() >=18 && labelFive !== undefined){
                labelSix.setVisible(true);
                labelSeven.setVisible(true);
            }
            if(map.getView().getZoom() <18 && labelFive !== undefined){
                labelSix.setVisible(false);
                labelSeven.setVisible(false);
            }
        }
    });
}
