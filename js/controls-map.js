/***** Controls ******/
var followTrack, followControl, exitControl, restartControl, activateMan, point, RotateNorthControl, clearControlButton, controlTimeMan, controlPointMan, myVar = [], enableMan = false, exitClick = false, interval = null, indexInterval = 0, indexPause = 0, indexNextPause = 0;
var cntrlIsPressed = false, moveAct = false, rangeChange = false, checkFollowCamera = true;
function rotateNorthControl() {
    var button = document.createElement('button');
    button.innerHTML = 'N';

    var handleRotateNorth = function (e) {
        map.getView().setRotation(0);
    };

    button.addEventListener('click', handleRotateNorth, false);

    var element = document.createElement('div');
    element.className = 'rotate-north ol-unselectable ol-control';
    element.appendChild(button);

    RotateNorthControl = new ol.control.Control({
        element: element
    });
    map.addControl(RotateNorthControl);
}

function clearControl() {
    var button = document.createElement('button');
    button.innerHTML = 'Limpiar mapa';

    var clearMapFunction = function (e) {
        map.removeControl(followControl);
        clearMap();
        $('div#seccion-altimetria').css('height', '0');
        $('.float').css('display', 'none');
    };
    button.addEventListener('click', clearMapFunction, false);
    var element = document.createElement('div');
    element.className = 'ol-clean ol-unselectable ol-control';
    element.appendChild(button);

    clearControlButton = new ol.control.Control({
        element: element
    });
    map.addControl(clearControlButton);

}

function changeViewControl() {
    const ol3d = new olcs.OLCesium({map: map});
    ol3d.setEnabled(true);

    scene = ol3d.getCesiumScene();
    Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI4YjYzZjc0OS1iZTJmLTQ1OWMtYjk2OC04MzA0MDM0NjNiY2EiLCJpZCI6NzI3Nywic2NvcGVzIjpbImFzciIsImdjIl0sImlhdCI6MTU0ODg3MjY0OX0.FlGmSyO4VmBUIWdmZ6SopcaRsvVrcqU8aZ2pWMm2xx8';
    scene.terrainProvider = new Cesium.CesiumTerrainProvider({
        url: Cesium.IonResource.fromAssetId(1),
        requestWaterMask: true,
        requestVertexNormals: true
    });
    scene.globe.depthTestAgainstTerrain = true;

    camera = ol3d.getCamera();
    camera.setTilt(0.1);


    var button = document.createElement('button');
    button.innerHTML = 'Animación';
    var element = document.createElement('div');
    element.className = 'follow-man ol-unselectable ol-control';
    element.id = 'activateMan';
    element.appendChild(button);
    followControl = new ol.control.Control({
        element: element
    });

    var entityChurch = churchCreator();
    //var entityTree = treeCreator();
    var arrayTrees = [[-15.518869, 28.113325], [-15.518783, 28.113372], [-15.518968, 28.113261]
                , [-15.534541, 28.086300], [-15.534476, 28.086293], [-15.534571, 28.086447], [-15.534455, 28.086423]
                , [-15.534295, 28.086370], [-15.534350, 28.086258]];


    activateMan = function () {
        $("#map").css("width", "85%");
        $("#radioVeloContainer").css("display", "block");
        $("#radiocontainer").css("display", "block");
        $("#check-container").css("display", "block");
        $("#radio0").prop("checked", true);
        $("#radio1Vel").prop("checked", true);
        $('#check-follow').prop('checked', true);
        checkFollowCamera = true;
        disabledForm();
        notInteractionMap();
        ol3d.getDataSourceDisplay().defaultDataSource.entities.removeAll();
        clearInterval(interval);
        modalSpinnerActive();
        removeControlActMan();
        restartMan();
        exitMan(ol3d);
        var position = Cesium.Cartesian3.fromDegrees(pointStart[0], pointStart[1]);
        var entityMan = manCreator(position);
        ol3d.getDataSourceDisplay().defaultDataSource.entities.add(entityMan);
        ol3d.getDataSourceDisplay().defaultDataSource.entities.add(entityChurch);
        for (var i = 0; i < arrayTrees.length; i++) {
            ol3d.getDataSourceDisplay().defaultDataSource.entities.add(treeCreator(arrayTrees[i], i));
        }
        //ol3d.getDataSourceDisplay().defaultDataSource.entities.add(entityTree);
        cameraActMan(pointStart);
        $("#p1").html("Rueda del ratón para acercarse/alejarse de la figura.<br>\
        Tecla ctrl + click y arrastrar para cambiar la perspectiva.");
        var pointsToFollow = allPoints(followTrack);
        var timing = $('input[name=speed]:checked').val();
        $("input[type=radio][name=speed]").change(function () {
            const val = $('input[name=speed]:checked').val();
            timing = val;
            clearInterval(interval);
            rangeChange = true;
            $('#check-follow').prop('checked', true);
            checkFollowCamera = true;
            startInterval(timing, pointsToFollow, ol3d, indexPause, indexNextPause);
        });
        $("input[type=radio][name=optradio]").change(function () {
            moveAct = true;
            var index = 0, indexNext = 0;
            const val = $('input[name=optradio]:checked').val();
            index = calculatePointPercent(val, pointsToFollow.length);
            index = Math.round(index);
            clearInterval(interval);
            $('#check-follow').prop('checked', true);
            checkFollowCamera = true;
            searchPoint:
                    for (var i = index; i < pointsToFollow.length; i++) {
                for (var j = 0; j < followTrack.length; j++) {
                    if (pointsToFollow[i] === followTrack[j]) {
                        indexNext = i;
                        break searchPoint;
                    }
                }
            }
            startInterval(timing, pointsToFollow, ol3d, index, indexNext);
        });
        $("#check-follow").on('change', function () {
            if ($(this).is(':checked')) {
                checkFollowCamera = true;
            } else {
                checkFollowCamera = false;
            }
        });
        controlTimeMan = setTimeout(function () {
            indexInterval = 0;
            indexNextPause = 0;
            $('#formControlRange').val(timing);
            startInterval(timing, pointsToFollow, ol3d, indexInterval, indexNextPause);
            camera.setDistance(10);
            camera.setTilt(1.4);
        }, 10000);
    };
    button.addEventListener('click', activateMan, false);
}

function startInterval(timing, pointsToFollow, ol3d, indexInterval, indexNextSegment) {
    var angle, angleCamera, angleNextSegment, cameraDistance;

    $(document).ready(function () {
        map.on('pointerdown', function () {
            if (cntrlIsPressed) {
                if (checkFollowCamera) {
                    camera.setDistance(cameraDistance);
                }
            }
        });
        $(document).keydown(function (event) {
            if (event.which === 17) {
                cntrlIsPressed = true;
                cameraDistance = camera.getDistance();
            }
        });
        $(document).keyup(function () {
            cntrlIsPressed = false;
        });
    });

    interval = setInterval(function () {

        if (moveAct === true) {
            angle = 360 - angleFromCoordinate(pointsToFollow[indexInterval], pointsToFollow[indexNextSegment]);
            ol3d.getDataSourceDisplay().defaultDataSource.entities.getById(11).orientation = updateOrientation(point, angle);
            angleCamera = angleFromCoordinate(pointsToFollow[indexInterval], pointsToFollow[indexNextSegment]);
            camera.setHeading(Cesium.Math.toRadians(angleCamera + 270));
            camera.setDistance(20);
            angleNextSegment = 0;
            moveAct = false;
        }
        point = Cesium.Cartesian3.fromDegrees(pointsToFollow[indexInterval][0], pointsToFollow[indexInterval][1]);
        ol3d.getDataSourceDisplay().defaultDataSource.entities.getById(11).position = point;
        var i = 0;
        while (i + 1 < followTrack.length) {
            if (pointsToFollow[indexInterval][0] === followTrack[i][0] && pointsToFollow[indexInterval][1] === followTrack[i][1]) {
                moveAct = false;
                indexNextSegment = pointsToFollow.indexOf(followTrack[i + 1]);
                angle = 360 - angleFromCoordinate(followTrack[i], followTrack[i + 1]);
                ol3d.getDataSourceDisplay().defaultDataSource.entities.getById(11).orientation = updateOrientation(point, angle);
                angleCamera = angleFromCoordinate(followTrack[i], followTrack[i + 1]);
                if (i + 2 < followTrack.length)
                    angleNextSegment = angleFromCoordinate(followTrack[i + 1], followTrack[i + 2]);
                if (checkFollowCamera) {
                    camera.setHeading(Cesium.Math.toRadians(angleCamera + 270));
                }
            }
            i = i + 1;
        }
        if (checkFollowCamera && camera.getDistance() > 150) {
            camera.setDistance(10);
        }
        indexInterval = indexInterval + 1;
        if (rangeChange) {
            angleNextSegment = 0;
            rangeChange = false;
        }
        if (checkFollowCamera) {
            if (angleNextSegment !== 0) {
                rotateCamera(indexNextSegment, indexInterval, angleCamera, angleNextSegment);
            }
            camera.setCenter(pointsToFollow[indexInterval]);
        }
        indexPause = indexInterval;
        indexNextPause = indexNextSegment;
        if (indexInterval === pointsToFollow.length) {
            camera.setCenter(pointsToFollow[pointsToFollow.length]);
            camera.setDistance(15);
            clearInterval(interval);
        }

    }, timing);
}


function removeControlActMan() {
    map.removeControl(restartControl);
    map.removeControl(exitControl);
    map.removeControl(followControl);
    map.removeControl(clearControlButton);
}

function cameraActMan(pointStart) {
    camera.setPosition(pointStart);
    camera.setTilt(1.4);
    camera.setDistance(50);
    camera.setHeading(1.57);
}

function exitMan(ol3d) {
    var button = document.createElement('button');
    button.innerHTML = 'Salir';
    var exitFunction = function (e) {
        $("#map").css("width", "100%");
        $("#radioVeloContainer").css("display", "none");
        $("#radiocontainer").css("display", "none");
        $("#check-container").css("display", "none");
        enabledForm();
        interactiveMap();
        ol3d.getDataSourceDisplay().defaultDataSource.entities.removeAll();
        clearInterval(interval);
        map.getView().setCenter(pointStart);
        map.getView().setZoom(14);
        camera.setTilt(0);
        map.addControl(followControl);
        map.addControl(clearControlButton);
        map.removeControl(exitControl);
        map.removeControl(restartControl);
        $("#p1").html("1. Clicke en el mapa para definir un punto de origen.<br>2. Clicke en el mapa para definir un punto de destino.<br><br> Tecla ctrl + click y arrastrar para cambiar la perspectiva.");
        map.un('pointerdown', function () {
            if (cntrlIsPressed) {
                if (checkFollowCamera) {
                    camera.setDistance(cameraDistance);
                }
            }
        });
        map.updateSize();
    };
    
    button.addEventListener('click', exitFunction, false);
    var element = document.createElement('div');
    element.className = 'exit-man ol-unselectable ol-control';
    element.id = 'exit-button-man';
    element.appendChild(button);
    exitControl = new ol.control.Control({
        element: element
    });
    map.addControl(exitControl);
}

function restartMan() {
    var button = document.createElement('button');
    button.innerHTML = 'Reiniciar';
    var element = document.createElement('div');
    element.className = 'restart-man ol-unselectable ol-control';
    element.id = 'Reiniciar';
    element.appendChild(button);
    restartControl = new ol.control.Control({
        element: element
    });
    button.addEventListener('click', activateMan);
    map.addControl(restartControl);
}

function rotateCamera(indexNextSegment, index, angleCamera, angleNextSegment) {
    var indexRotate10 = 10, indexRotate7 = 7, indexRotate5 = 5, indexRotate3 = 3, indexRotate1 = 1;
    if (angleCamera === 0)
        return false;
    if (angleNextSegment === 0)
        return false;
    if (indexNextSegment - indexRotate10 === index) {
        if (angleCamera + 270 < angleNextSegment + 270) {
            var subs = (angleNextSegment + 270) - (angleCamera + 270);
            return camera.setHeading(Cesium.Math.toRadians(angleCamera + (subs / 6) + 270));
        } else {
            var subs = (angleCamera + 270) - (angleNextSegment + 270);
            return camera.setHeading(Cesium.Math.toRadians(angleCamera - (subs / 6) + 270));
        }
    }
    if (indexNextSegment - indexRotate7 === index) {
        if (angleCamera + 270 < angleNextSegment + 270) {
            var subs = (angleNextSegment + 270) - (angleCamera + 270);
            return camera.setHeading(Cesium.Math.toRadians(angleCamera + (subs / 4) + 270));
        } else {
            var subs = (angleCamera + 270) - (angleNextSegment + 270);
            return camera.setHeading(Cesium.Math.toRadians(angleCamera - (subs / 4) + 270));
        }
    }

    if (indexNextSegment - indexRotate5 === index) {
        if (angleCamera + 270 < angleNextSegment + 270) {
            var subs = (angleNextSegment + 270) - (angleCamera + 270);
            return camera.setHeading(Cesium.Math.toRadians(angleCamera + (subs / 3) + 270));
        } else {
            var subs = (angleCamera + 270) - (angleNextSegment + 270);
            return camera.setHeading(Cesium.Math.toRadians(angleCamera - (subs / 3) + 270));
        }
    }

    if (indexNextSegment - indexRotate3 === index) {
        if (angleCamera + 270 < angleNextSegment + 270) {
            var subs = (angleNextSegment + 270) - (angleCamera + 270);
            return camera.setHeading(Cesium.Math.toRadians(angleCamera + (subs / 2) + 270));
        } else {
            var subs = (angleCamera + 270) - (angleNextSegment + 270);
            return camera.setHeading(Cesium.Math.toRadians(angleCamera - (subs / 2) + 270));
        }
    }

    if (indexNextSegment - indexRotate1 === index) {
        if (angleCamera + 270 < angleNextSegment + 270) {
            var subs = (angleNextSegment + 270) - (angleCamera + 270);
            return camera.setHeading(Cesium.Math.toRadians(angleCamera + (subs / 1.25) + 270));
        } else {
            var subs = (angleCamera + 270) - (angleNextSegment + 270);
            return camera.setHeading(Cesium.Math.toRadians(angleCamera - (subs / 1.25) + 270));
        }
    }
}

function updateOrientation(position, degrees) {
    var heading = Cesium.Math.toRadians(degrees);
    var pitch = 0;
    var roll = 0;
    var hpr = new Cesium.HeadingPitchRoll(heading, pitch, roll);
    var orientation = Cesium.Transforms.headingPitchRollQuaternion(position, hpr);
    return orientation;
}

function churchCreator() {
    var position = Cesium.Cartesian3.fromDegrees(-15.523039, 28.118543);
    var heading = Cesium.Math.toRadians(85);
    var pitch = 0;
    var roll = 0;
    var hpr = new Cesium.HeadingPitchRoll(heading, pitch, roll);
    var orientation = Cesium.Transforms.headingPitchRollQuaternion(position, hpr);
    var entity = {
        id: 12,
        name: 'ChurchArucas',
        position: position,
        model: {
            uri: 'CesiumMan/iglesia.gltf',
            scale: 0.85,
            heightReference: Cesium.HeightReference.CLAMP_TO_GROUND
        },
        orientation: orientation
    };
    return entity;
}

function treeCreator(pos, i) {
    var position = Cesium.Cartesian3.fromDegrees(pos[0], pos[1]);
    var entity = {
        id: 20 + i,
        name: 'tree',
        position: position,
        model: {
            uri: 'CesiumMan/scene.glb',
            scale: 1,
            heightReference: Cesium.HeightReference.CLAMP_TO_GROUND
        }

    };
    return entity;
}

function manCreator(position) {
    var heading = Cesium.Math.toRadians(90);
    var pitch = 0;
    var roll = 0;
    var hpr = new Cesium.HeadingPitchRoll(heading, pitch, roll);
    var orientation = Cesium.Transforms.headingPitchRollQuaternion(position, hpr);
    var entity = {
        id: 11,
        name: 'CesiumMan',
        position: position,
        model: {
            uri: 'CesiumMan/Cesium_Man.gltf',
            scale: 1,
            heightReference: Cesium.HeightReference.CLAMP_TO_GROUND
        },
        orientation: orientation

    };
    return entity;
}

function modalSpinnerActive() {
    $('.modal-spinner').modal({backdrop: 'static', keyboard: false});
    setTimeout(function () {
        $('.modal-spinner').modal('hide');
    }, 10000);
}

function disabledForm() {
    $('#originTown').prop('disabled', true);
    $('#origen').prop('disabled', true);
    $('#destinationTown').prop('disabled', true);
    $('#destino').prop('disabled', true);
    $('#button-form').prop('disabled', true);
}

function enabledForm() {
    $('#originTown').prop('disabled', false);
    $('#origen').prop('disabled', false);
    $('#destinationTown').prop('disabled', false);
    $('#destino').prop('disabled', false);
    $('#button-form').prop('disabled', false);
}


function clearMap() {
    var layersToRemove = [];
    map.getLayers().forEach(function (layer) {
        if (layer.get('title') === undefined) {
            if (layer.get('name') === undefined) {
                layersToRemove.push(layer);
            }
        }
    });
    origen = null;
    var len = layersToRemove.length;
    for (var i = 0; i < len; i++) {
        map.removeLayer(layersToRemove[i]);
    }
}
