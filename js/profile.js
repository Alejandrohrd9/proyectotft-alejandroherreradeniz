
function loadData(data){
    var rows = data.split(/\r?\n|\r/);
    for (var i = 0; i < rows.length-1; i++) {
        var rowsCell = rows[i].split(',');
        table.push(rowsCell);
    }
}
function drawOriginalProfile(){
    var texto='<div id="accordion" class="detalles-sendero"><div class="card"><div class="card-header" id="headingOne"><h5 class="mb-0"><a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Altimetría sendero&darr;</a></h5></div><div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion"><div class="card-body"><svg id="profile-graph" class="graph"></svg></div></div></div></div>';
    $('div#seccion-altimetria').css('height','auto');
    $('#seccion-altimetria').html(texto);
    createProfile(coordinatesTrack,'#profile-graph',pointStart);
    coordinatesTrack = [];
}

function createProfile(track,id,startingPoint){
    var coordinates = getVectorCoordinates(track,startingPoint), heights = new Array(), distances = new Array(), data = [];
    for (var i = 0; i < coordinates.length; i++) {
        var row = getRow(coordinates[i][1]);
        var colum = getColum(coordinates[i][0]);
        heights[i] = table[row][colum];
        if(i === 0){
            distances[0] = 0;
            data.push({distancia: +0, altura: +heights[i]});
        }else{
            var distance = distances[i-1]+getGeometryDistance(coordinates[i-1][1],coordinates[i-1][0],coordinates[i][1],coordinates[i][0]);
            distances[i] = distance;
            data.push({distancia:+distances[i], altura: +heights[i]});
        }
    }
    drawProfile(data,id);
    
}

function drawProfile(data,id){
    var svgWidth = 600, svgHeight = 400;
    var margin = { top: 20, right: 20, bottom: 30, left: 50 };
    var width = svgWidth - margin.left - margin.right;
    var height = svgHeight - margin.top - margin.bottom;
    var svg = d3.select(id)
     /*.attr("width", svgWidth)
     .attr("height", svgHeight);*/
        .attr("viewBox",'0 0 600 400')
        .attr("preserveAspectRatio", "xMinYMid");
    var g = svg.append("g")
    .attr("transform", 
      "translate(" + margin.left + "," + margin.top + ")"
   );
    var x = d3.scaleLinear().rangeRound([0, width]);
    var y = d3.scaleLinear().rangeRound([height, 0]);
    var line = d3.line()
    .curve(d3.curveBasis)
    .x(function(d) { return x(d.distancia)})
    .y(function(d) { return y(d.altura)});
    
    x.domain(d3.extent(data, function(d) { return d.distancia }));
    //   y.domain(d3.extent(datos, function(d) { return d.altura }));
    y.domain([d3.min(data,function(d){return d.altura}),d3.max(data,function(d){return d.altura})+20]);
   
   var area = d3.area()
    .curve(d3.curveBasis)
    .x(function(d) { return x(d.distancia); })
    .y0(height)
    .y1(function(d) { return y(d.altura); });
      
    g.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x))
    .append("text")
    .attr("fill", "#000")
    .attr("transform", "rotate(0)")
    .attr("x", 535)
    .attr("dy", "2.7em")
    .attr("text-anchor", "end")
    .text("Distancia (km)");
    
    g.append("g")
    .call(d3.axisLeft(y))
    .append("text")
    .attr("fill", "#000")
    .attr("transform", "rotate(-90)")
    //   .attr("y", 6)
    //   .attr("dy", "0.71em")
    .attr("x", -20)
    .attr("dy", "-3.5em")
    .attr("text-anchor", "end")
    .text("Altura (m)");
   
    g.append("path")
    .datum(data)
    .attr("fill", "none")
    .attr("stroke", "steelblue")
    .attr("stroke-linejoin", "round")
    .attr("stroke-linecap", "round")
    .attr("stroke-width", 1.5)
    .attr("d", line);
   
    g.append("path")
    .datum(data)
    .attr("fill","none")
    .attr("d",area);
    
}

function getVectorCoordinates(userTrack, startingPoint){
    var distance1,distance2,stretch,feature, array = [];
    for (var i = 0; i < userTrack.length; i++) {
        stretch = userTrack[i].features;
        for (var j = 0; j < stretch.length; j++) {
            feature = stretch[j].geometry.coordinates;
            if(i===0 && j===0){
                distance1 = getGeometryDistance(startingPoint[1],startingPoint[0],feature[0][1],feature[0][0]); //al primer punto del primer tramo
                distance2 = getGeometryDistance(startingPoint[1],startingPoint[0],feature[feature.length-1][1],feature[feature.length-1][0]); //hasta el último punto del primer tramo
                array = createTrack(feature,distance1,distance2,array);
            }else{
                distance1 = getGeometryDistance(array[array.length-1][1],array[array.length-1][0],feature[0][1],feature[0][0]);
                distance2 = getGeometryDistance(array[array.length-1][1],array[array.length-1][0],feature[feature.length-1][1],feature[feature.length-1][0]);
                array = createTrack(feature,distance1,distance2,array);
            }
        }
    }
    followTrack = array;
    return array;
}

function createTrack(stretch,distance1,distance2,finalTrack){
    if(distance1<distance2){
        for (var i = 0; i < stretch.length; i++) {
            finalTrack.push(stretch[i]);
        }
    }else{
        for (var i = stretch.length-1; i >= 0; i--) {
            finalTrack.push(stretch[i]);
        }
    } 
    return finalTrack;
}

function convertToRadians(value){
    return (Math.PI/180)*value;
}

function getGeometryDistance(lat1,lon1,lat2,lon2){
    if(lat1===lat2 && lon1===lon2)return 0;
  
    var angularDistance = Math.acos(Math.cos(convertToRadians(90)-convertToRadians(lat2))*Math.cos(convertToRadians(90)-convertToRadians(lat1))+Math.sin(convertToRadians(90)-convertToRadians(lat2))*Math.sin(convertToRadians(90)-convertToRadians(lat1))*Math.cos(convertToRadians(lon2-lon1)));
    return angularDistance*6378.137;
}


function interpolacion_lineal(len,len0,len1,width){
    return Math.round((len-len0)/(len1-len0)*width);
}

function getColum(longitude){
    return interpolacion_lineal(longitude,-15.5864985659999995,-15.4762375849999998,725);
}

function getRow(latitude){
    return interpolacion_lineal(latitude,28.1626316989999985,28.0693209520000018,686);
}