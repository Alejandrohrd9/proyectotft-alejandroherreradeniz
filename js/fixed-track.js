
function addLayerIntersect(text) {
    var json = plainTextToJson(text);
    var source = new ol.source.Vector({
        features: (new ol.format.GeoJSON()).readFeatures(json)
    });


    var styleDash = new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'yellow', width: 4,lineDash: [40,40]})
    });
    
    return new ol.layer.Vector({
        source: source,
        style: styleDash,
        altitudeMode: 'clampToGround'
    });

}


function createJsonIntersect(point1, point2) {
    var header = '{"type": "FeatureCollection", "name": "", "crs": {"type": "name","properties": {"name": "EPSG:4326"}},"features": [{"type": "Feature",' +
            '"geometry":{"type":"LineString","coordinates":[';
    var close = ']}}]}';

    return header + "[" + point1[0] + "," + point1[1] + "],[" + point2[0] + "," + point2[1] + "]" + close;
}

