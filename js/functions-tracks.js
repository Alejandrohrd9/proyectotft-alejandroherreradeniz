var map;
var oldLayer;
var oldStartMark;
var oldEndMark;
var secondMark = false;
var origen = null;
var destino = null;
var originIcon;
var destinyIcon;
var first;
var second;
var date, scene, coordinatesTrack = new Array(), pointStart = null;

function initMap() {
    var view = new ol.View({
        projection: 'EPSG:4326',
        center: [-15.6, 28.0],
        zoom: 7,
        maxZoom: 21,
        minZoom: 10

    });

    var municipios = addLayerMunicipioGeoJson();
    var firgasRoads = addLayerFirgasGeoJson();
    var arucasRoads = addLayerArucasGeoJson();

    map = new ol.Map({
        target: 'map',
        layers: [new ol.layer.Group({
                'title': 'Mapas',
                layers: [
                    new ol.layer.Tile({
                        title: 'OrtoExpress',
                        crossOrigin: "anonymous",
                        source: new ol.source.TileWMS({
                            url: 'http://idecan3.grafcan.es/ServicioWMS/3D/OrtoExpress_bat?',
                            params: {
                                'LAYERS': ['ortoexpress,batimetria'],
                                'FORMAT': 'image/jpeg'
                            }
                        }),
                        type: 'base'
                    }),
                    new ol.layer.Tile({
                        title: 'Mapa topográfico',
                        source: new ol.source.TileWMS({
                            url: 'https://idecan2.grafcan.es/ServicioWMS/MTI?',
                            params: {
                                'LAYERS': ['MUN', 'CONS', 'FONDO', 'HID', 'VIAL', 'CN', 'PA'],
                                'FORMAT': 'image/png'
                            }
                        }),
                        type: 'base',
                        visible: false,
                    }),

                    new ol.layer.Tile({
                        title: 'Modelo de terreno',
                        source: new ol.source.TileWMS({
                            url: 'https://idecan1.grafcan.es/ServicioWMS/MTL?',
                            params: {
                                'LAYERS': 'LIDAR_MTL',
                                'FORMAT': 'image/jpeg'
                            }
                        }),
                        type: 'base',
                        visible: false
                    }),
                    new ol.layer.Tile({
                        title: 'Open Street Map',
                        source: new ol.source.OSM(),
                        type: 'base',
                        visible: false
                    })
                ]
            }),
            new ol.layer.Group({
                'title': 'Contenido',
                layers: [
                    municipios,
                    firgasRoads,
                    arucasRoads
                ]
            })

        ]
    });

    map.addControl(new ol.control.LayerSwitcher());

    map.setView(view);

    clearControl();
    rotateNorthControl();
    changeViewControl();
    zoomLabelsMap();
    interactiveMap();
    return map;
}

var callback = function (evt) {
    if (secondMark === false || origen === null) {
        clearMap();
        origen = startMark(evt);
        $(document).ready(function () {
            $("#modalSecondPoint").modal("show");
            map.removeControl(followControl);
        });
    } else {
        destino = endMark(evt);
        var result = getCaminoClickMapa();
        if (result === "null") {
            $(document).ready(function () {
                $("#miModal").modal("show");
            });
            clearMap();
        } else {
            var parts = result.split("|");
            var caminoJson = parts[0];
            var obj = $.parseJSON(parts[1]);
            var coordinates = obj['coordinates'];
            pointStart = coordinates;
            addRoadLayer(caminoJson);
            $(document).ready(function () {
                map.addControl(followControl);
            });
            map.addLayer(addLayerIntersect(createJsonIntersect(origen, followTrack[0])));
            map.addLayer(addLayerIntersect(createJsonIntersect(followTrack[followTrack.length - 1], destino)));

        }
    }
};

function interactiveMap() {
    map.on('singleclick', callback);
}

function notInteractionMap() {
    map.un('singleclick', callback);
}

/*Icono inicio*/
function getStartingPoint(lonLat) {
    var startingPoint = new ol.Feature({
        geometry: new ol.geom.Point(lonLat),
    });

    var startIconStyle = new ol.style.Style({
        image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
            anchor: [14, 30],
            anchorXUnits: 'pixels',
            anchorYUnits: 'pixels',
            scale: 1,
            src: 'img/map-localization-start.svg'
        }))
    });

    startingPoint.setStyle(startIconStyle);
    return startingPoint;
}

/*Icono inicio*/
function addStartPointTrack(lonLat) {
    var vectorSource = new ol.source.Vector({
        features: [getStartingPoint(lonLat)]
    });

    return new ol.layer.Vector({
        source: vectorSource,
        altitudeMode: 'clampToGround'
    });
}

/*Icono final*/
function getFinalPoint(lonLat) {
    var finalPoint = new ol.Feature({
        geometry: new ol.geom.Point(lonLat),
    });

    var finalIconStyle = new ol.style.Style({
        image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
            anchor: [14, 30],
            anchorXUnits: 'pixels',
            anchorYUnits: 'pixels',
            scale: 1,
            src: 'img/map-localization-fin.svg'
        }))
    });

    finalPoint.setStyle(finalIconStyle);
    return finalPoint;
}

/*Icono final*/
function addFinalPointTrack(lonLat) {
    var vectorSource = new ol.source.Vector({
        features: [getFinalPoint(lonLat)]
    });

    return new ol.layer.Vector({
        source: vectorSource,
        altitudeMode: 'clampToGround'
    });
}

/*Primer click*/
function startMark(evt) {
    originIcon = addStartPointTrack(evt.coordinate);
    if (first !== undefined) {
        map.removeLayer(first);
    }
    map.addLayer(originIcon);
    secondMark = true;
    return evt.coordinate;
}

/*Segundo click*/
function endMark(evt) {
    destinyIcon = addFinalPointTrack(evt.coordinate);
    if (second !== undefined) {
        map.removeLayer(second);
    }
    map.addLayer(destinyIcon);
    secondMark = false;
    return evt.coordinate;
}

/*Caminos Firgas*/
function addLayerFirgasGeoJson() {
    var geojsonObject = {
        'type': 'FeatureCollection',
        'crs': {
            'type': 'name',
            'properties': {
                'name': 'EPSG:4326'
            }
        },
        'features': [{
                'type': "Feature",
                'geometry': plainTextToJson(getGeomFirgas())
            }]
    };
    var trackStyleFirgas = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: '#04B486',
            width: 2.5
        })
    });

    var source = new ol.source.Vector({
        features: (new ol.format.GeoJSON()).readFeatures(geojsonObject)
    });

    return new ol.layer.Vector({
        title: 'Firgas',
        source: source,
        style: trackStyleFirgas,
        altitudeMode: 'clampToGround'
    });
}

/*Caminos Arucas*/
function addLayerArucasGeoJson() {
    var geojsonObject = {
        'type': 'FeatureCollection',
        'crs': {
            'type': 'name',
            'properties': {
                'name': 'EPSG:4326'
            }
        },
        'features': [{
                'type': "Feature",
                'geometry': plainTextToJson(getGeomArucas())
            }]
    };
    var trackStyleFirgas = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: '#04B486',
            width: 2.5
        })
    });

    var source = new ol.source.Vector({
        features: (new ol.format.GeoJSON()).readFeatures(geojsonObject)
    });

    return new ol.layer.Vector({
        title: 'Arucas',
        source: source,
        style: trackStyleFirgas,
        altitudeMode: 'clampToGround'
    });
}

/*Delimitador municipio*/
function addLayerMunicipioGeoJson() {
    var geojsonObject = {
        'type': 'FeatureCollection',
        'crs': {
            'type': 'name',
            'properties': {
                'name': 'EPSG:4326'
            }
        },
        'features': [{
                'type': "Feature",
                'geometry': plainTextToJson(getGeomMunicipio())
            }]
    };
    var trackStyleMunicipio = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'orange',
            width: 1
        })
    });

    var source = new ol.source.Vector({
        features: (new ol.format.GeoJSON()).readFeatures(geojsonObject)
    });

    return new ol.layer.Vector({
        title: 'Municipios',
        source: source,
        style: trackStyleMunicipio,
        altitudeMode: 'clampToGround'
    });
}

/*Caminos creados*/
function addLayerCaminoGeoJson(text) {
    coordinatesTrack = [];
    var json = plainTextToJson(text);
    var source = new ol.source.Vector({
        features: (new ol.format.GeoJSON()).readFeatures(json)
    });
    coordinatesTrack.push(json);
    return new ol.layer.Vector({
        source: source,
        style: new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'red',
                width: 5
            })
        }),
        altitudeMode: 'clampToGround'
    });
}

/*Consulta caminos Firgas*/
function getGeomFirgas() {
    var ajax = new XMLHttpRequest();
    ajax.open("POST", "query/caminosFirgas.php", false);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send();
    return ajax.responseText;
}

/*Consulta caminos Arucas*/
function getGeomArucas() {
    var ajax = new XMLHttpRequest();
    ajax.open("POST", "query/caminosArucas.php", false);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send();
    return ajax.responseText;
}

/*Consulta delimitador municipio*/
function getGeomMunicipio() {
    var ajax = new XMLHttpRequest();
    ajax.open("POST", "query/delimitadorMunicipio.php", false);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send();
    return ajax.responseText;
}

/*Consulta camino*/
function getCamino() {
    var ajax = new XMLHttpRequest();
    ajax.open("POST", "query/caminosForm.php", false);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var var1 = document.getElementById("origen").value;
    var var2 = document.getElementById("destino").value;
    ajax.send("origen=" + var1 + "&destino=" + var2);

    return ajax.responseText;
}

/*Consulta camino raton*/
function getCaminoClickMapa() {
    var ajax = new XMLHttpRequest();
    ajax.open("POST", "query/caminosMap.php", false);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("origen=" + origen + "&destino=" + destino);
    return ajax.responseText;
}

/*Añadir camino creado*/
function addRoadLayer(text) {
    nuevoLayer = addLayerCaminoGeoJson(text);
    map.addLayer(nuevoLayer);
    drawOriginalProfile();
    $('.float').css('display', 'block');
}

function plainTextToJson(text) {
    return JSON.parse(text);
}

/*Vector icono inicio*/
function addStartPointTrackJson(lonLat) {
    var obj = $.parseJSON(lonLat);
    var coordinates = obj['coordinates'];
    pointStart = coordinates;
    var startingPoint = new ol.Feature({
        geometry: new ol.geom.Point(coordinates),
        name: 'Null Island',
        population: 4000,
        rainfall: 500
    });
    var startIconStyle = new ol.style.Style({
        image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
            anchor: [14, 30],
            anchorXUnits: 'pixels',
            anchorYUnits: 'pixels',
            scale: 1,
            src: 'img/map-localization-start.svg'
        }))
    });

    startingPoint.setStyle(startIconStyle);

    var vectorSource = new ol.source.Vector({
        features: [startingPoint]
    });

    return new ol.layer.Vector({
        source: vectorSource,
        altitudeMode: 'clampToGround'
    });
}

//Vector del icono final
function addFinalPointTrackJson(lonLat) {
    var obj = $.parseJSON(lonLat);
    var coordinates = obj['coordinates'];
    var finalPoint = new ol.Feature({
        geometry: new ol.geom.Point(coordinates),
        name: 'Null Island',
        population: 4000,
        rainfall: 500
    });

    var finalIconStyle = new ol.style.Style({
        image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
            anchor: [14, 30],
            anchorXUnits: 'pixels',
            anchorYUnits: 'pixels',
            scale: 1,
            src: 'img/map-localization-fin.svg'
        }))
    });

    finalPoint.setStyle(finalIconStyle);

    var vectorSource = new ol.source.Vector({
        features: [finalPoint]
    });

    return new ol.layer.Vector({
        source: vectorSource,
        altitudeMode: 'clampToGround'
    });
}

//Añadir puntos form
function addSelectorPoints(geojson, start, end) {
    if (geojson !== "") {
        clearMap();
        first = addStartPointTrackJson(start);
        map.addLayer(first);
        second = addFinalPointTrackJson(end);
        map.addLayer(second);
        addRoadLayer(geojson);
    }
}

function puntosComprobar(point) {
    var punto = new ol.Feature({
        geometry: new ol.geom.Point(point),
        name: 'Null Island',
        population: 4000,
        rainfall: 500
    });

    var estilo = new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(255, 100, 50, 0.3)'
        }),
        stroke: new ol.style.Stroke({
            width: 2,
            color: 'rgba(255, 100, 50, 0.8)'
        }),
        image: new ol.style.Circle({
            fill: new ol.style.Fill({
                color: 'rgba(55, 200, 150, 0.8)'
            }),
            stroke: new ol.style.Stroke({
                width: 1,
                color: 'rgba(55, 200, 150, 0.8)'
            }),
            radius: 2
        })
    });

    punto.setStyle(estilo);
    return punto;
}

/*Icono final*/
function añadirPuntoComprobacion(lonLat) {
    var vectorSource = new ol.source.Vector({
        features: [puntosComprobar(lonLat)]
    });

    return new ol.layer.Vector({
        source: vectorSource,
        altitudeMode: 'clampToGround'
    });
}


function getAltitudeRaster(lon, lat) {
    var row = getRow(lat);
    var colum = getColum(lon);
    return  plainTextToJson((table[row][colum]));

}

