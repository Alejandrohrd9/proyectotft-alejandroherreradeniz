function difference(point1, point2) {
    return point2 - point1;
}

function lineParametricEquation(lon1, lat1, lon2, lat2, t, d) {
    var p1 = geographicToUTM(lon1, lat1);
    var p2 = geographicToUTM(lon2, lat2);
    var values = [];
    var x = p1[0] + (p2[0] - p1[0]) * (t / d);
    var y = p1[1] + (p2[1] - p1[1]) * (t / d);
    values[0] = x;
    values[1] = y;

    return values;
}

function geographicToUTM(lon, lat) {
    var values = [];
    var lon0 = -16.6;
    var lat0 = 28.2;
    var rEarth = 6371000;
    var x = rEarth * (lon - lon0) * Math.cos(degreesToRadians(lat0));
    var y = rEarth * (lat - lat0);
    values[0] = x;
    values[1] = y;
    return values;
}

function UTMtoGeographic(x, y) {
    var values = [];
    var rEarth = 6371000;
    var lon0 = -16.6;
    var lat0 = 28.2;
    var lon = (x + ((rEarth * Math.cos(degreesToRadians(lat0))) * lon0)) / (rEarth * Math.cos(degreesToRadians(lat0)));
    var lat = (y + lat0 * rEarth) / rEarth;
    values[0] = lon;
    values[1] = lat;
    return values;
}

function distance(p1, p2) {
    var point1 = geographicToUTM(p1[0], p1[1]);
    var point2 = geographicToUTM(p2[0], p2[1]);
    var coordX = Math.pow((point2[0] - point1[0]), 2);
    var coordY = Math.pow((point2[1] - point1[1]), 2);
    var d = Math.sqrt(coordX + coordY);
    return d;
}

function allPoints(followTrack) {
    var arrayP = [];
    var i = 0;
    while (i + 1 < followTrack.length) {
        arrayP.push(followTrack[i]);
        var d = distance(followTrack[i], followTrack[i + 1]);
        //console.log(d);
        for (var t = 0; t < d; t = t + 20) {
            var equations = lineParametricEquation(followTrack[i][0], followTrack[i][1], followTrack[i + 1][0], followTrack[i + 1][1], t, d);
            var geographicPoints = UTMtoGeographic(equations[0], equations[1]);
            arrayP.push(geographicPoints);
        }
        i = i + 1;
    }
    arrayP.push(followTrack[i]);

    return arrayP;
}

function angleFromCoordinate(point1,point2) {
    var p1 = {
        x: point1[0],
        y: point1[1]
    };

    var p2 = {
        x: point2[0],
        y: point2[1]
    };
    var angle = Math.atan2(p2.y - p1.y, p2.x - p1.x);
    var angleDeg = radiansToDegrees(angle);
    return angleDeg;
}

function calculatePointPercent(percent,totalPoints){
    var res = (totalPoints*percent)/100;
    return res;
}

function degreesToRadians(degreesValue) {
    return Math.PI / 180 * degreesValue;
}

function radiansToDegrees(radiansValue) {
    return radiansValue * (180 / Math.PI);
}