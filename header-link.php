<!--jQuery-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<!-- OL -->
<script src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<link rel="stylesheet" href="ol3-layerswitcher-master/src/ol3-layerswitcher.css" />
<script src="ol3-layerswitcher-master/src/ol3-layerswitcher.js"></script>
<!--Fichero personal css-->
<link rel="stylesheet" type="text/css" href="estilos/style.css">
<link rel="stylesheet" href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css" type="text/css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<!--Funciones-->
<script language="javascript" type="text/javascript" src="js/label-functions.js"></script>
<script language="javascript" type="text/javascript" src="js/controls-map.js"></script>
<script language="javascript" type="text/javascript" src="js/profile.js"></script>
<script language="javascript" type="text/javascript" src="js/functions-tracks.js"></script>
<script language="javascript" type="text/javascript" src="js/math-functions.js"></script>
<script language="javascript" type="text/javascript" src="js/fixed-track.js"></script>
<!--Cesium-->
<link rel="stylesheet" href="ol-cesium/olcs.css">
<script src="ol-cesium/node_modules/@camptocamp/cesium/Build/CesiumUnminified/Cesium.js"></script>
<script src="ol-cesium/olcesium.js"></script>
<!--d3.js-->
<script src="https://d3js.org/d3.v5.min.js"></script>
<!--<script src="ol-cesium/Cesium/Cesium.js"></script>-->
<!--<style>@import url(../templates/bucket.css);</style>-->