<div class="main-first-wrapper">
    <nav class="navbar navbar-expand-lg  navbar-dark bg-dark">
        <div>
            <img id="logo-web" src="img/logo-edit.png">
            <a class="navbar-brand" href="index.php">Camina y Descubre Gran Canaria</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav ml-md-auto d-md-flex">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Inicio
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="map.php" role="button" aria-haspopup="true">Senderos</a>
                </li>
            </ul>
        </div>
    </nav>
</div>