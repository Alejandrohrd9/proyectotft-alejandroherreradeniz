<?php

$posts = array($_POST["origen"], $_POST["destino"]);

include '../db_functions.php';
$connection = db_conection();

$sqlFirstPoint = " SELECT v.id,st_AsGeoJson(v.the_geom) as geom FROM ruta_noded_vertices_pgr AS v,ruta_noded AS e "
        . "WHERE e.source=v.id OR e.target=v.id ORDER BY v.the_geom <-> ST_SetSRID(ST_MakePoint($posts[0]),4326) LIMIT 1";
$rowVerticeStart = pg_fetch_row(pg_query($sqlFirstPoint));

$sqlSecondPoint = " SELECT v.id,st_asgeojson(v.the_geom) as geom FROM ruta_noded_vertices_pgr AS v,ruta_noded AS e "
        . "WHERE e.source=v.id OR e.target=v.id ORDER BY v.the_geom <-> ST_SetSRID(ST_MakePoint($posts[1]),4326) LIMIT 1";
$rowVerticeFinal = pg_fetch_row(pg_query($sqlSecondPoint));

$header = '{"type": "FeatureCollection", "name": "", "crs": {"type": "name","properties": {"name": "EPSG:4326"}},'
        . '"features": [';
$close = ']}';

$sql = "SELECT st_asgeojson(r.the_geom) AS the_geom,r.punto_inicial, r.punto_final FROM pgr_dijkstra('SELECT id, "
        . "source, target,distancia as cost FROM ruta_noded',$rowVerticeStart[0], $rowVerticeFinal[0], false) AS d,ruta_noded AS r WHERE d.edge=r.id";
$result = pg_query($sql);
if (!$result) {
    echo "Ocurrió un error!";
    exit;
}
if (pg_num_rows($result) == 0) {
    echo "null";
} else {
    $row = pg_fetch_all($result);
    foreach ($row as $value) {
        $header .= '{"type": "Feature", ';
        $header .= '"geometry": ' . $value['the_geom'] . '},';
    }
    $header = trim($header, ',');
    $header .= $close;
    echo "$header|$rowVerticeStart[1]|$rowVerticeFinal[1]";
}
