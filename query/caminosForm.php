
        
 
<?php
    $posts = array($_POST["origen"], $_POST["destino"]);

    
    include '../db_functions.php';
    $connection = db_conection();
    
    $startPoint = "SELECT st_asgeojson(st_transform(geom, 4326)) FROM punto WHERE nombre='$posts[0]'";
    $resultStartPoint = pg_query($startPoint);
    $rowStartPoint = pg_fetch_row($resultStartPoint);
    
    $arrS = json_decode($rowStartPoint[0], true);
    foreach ($arrS['coordinates'] as $value) { 
             $resJsonS.=implode(",",$value);
    }
    $lonLatStart = $resJsonS;
    
    $finalPoint = "SELECT st_asgeojson(st_transform(geom, 4326)) FROM punto WHERE nombre='$posts[1]'";
    $resultFinalPoint = pg_query($finalPoint);
    $rowFinalPoint = pg_fetch_row($resultFinalPoint);
    
    $arrF = json_decode($rowFinalPoint[0],true);
    foreach ($arrF['coordinates'] as $value) {
        $resJsonF.= implode(",", $value);
    }
    $lonLatFinal = $resJsonF;
    
    $header = '{"type": "FeatureCollection", "name": "", "crs": {"type": "name","properties": {"name": "EPSG:4326"}},'
            . '"features": [';
    $close = ']}';

    //Funcional
    $sqlFirstPoint = " SELECT v.id,st_AsGeoJson(v.the_geom) as geom FROM ruta_noded_vertices_pgr "
            . "AS v,ruta_noded AS e WHERE e.source=v.id OR e.target=v.id "
            . "ORDER BY v.the_geom <-> ST_SetSRID(ST_MakePoint($lonLatStart),4326) LIMIT 1";
    $rowVerticeStart = pg_fetch_row(pg_query($sqlFirstPoint));
   
    $sqlSecondPoint = " SELECT v.id,st_asgeojson(v.the_geom) as geom FROM ruta_noded_vertices_pgr "
            . "AS v,ruta_noded AS e WHERE e.source=v.id OR e.target=v.id "
            . "ORDER BY v.the_geom <-> ST_SetSRID(ST_MakePoint($lonLatFinal),4326) LIMIT 1";
    $rowVerticeFinal = pg_fetch_row(pg_query($sqlSecondPoint));
    
    $sql = "SELECT st_asgeojson(r.the_geom) AS the_geom,r.punto_inicial, r.punto_final "
            . "FROM pgr_dijkstra('SELECT id, source, target,distancia as cost "
            . "FROM ruta_noded',$rowVerticeStart[0], $rowVerticeFinal[0], false) AS d,ruta_noded AS r WHERE d.edge=r.id";
    $result = pg_query($sql);
    if(!$result){
        echo "Ocurrió un error!";
        exit;
    }

    $row = pg_fetch_all($result);
    foreach ($row as $value) {
        $header .= '{"type": "Feature", ';
        $header .='"geometry": ' .$value['the_geom'].'},';
    }
    $header = trim($header,',');
    $header.=$close;

    foreach ($row as $value) {
        $start = $value['r.punto_inicial'];
    }
    
    foreach ($row as $value) {
        $end = $value['r.punto_final'];
    }
    
    echo "$header|$rowVerticeStart[1]|$rowVerticeFinal[1]";
    ?>

