<div class="main-wrapper footer-container">
            <footer>
                <div class="footer">
                    <div class="footer-menu">
                        <ul id="ul-footer">
                            <a href="cookies.php" target="_blank"><li class="li-footer">Cookies</li></a>
                            <a href="#" target="_blank"><li class="li-footer">Política de Privacidad</li></a>
                            <a href="#" target="_blank"><li  class="li-footer">Aviso legal</li></a>
                        </ul>
                    </div>
                    <div class="copy-right-text">Copyright &copy; 2019 All rights reserved</div>
                </div>
            </footer>
        </div>