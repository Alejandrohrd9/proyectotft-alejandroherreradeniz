

<div class="container">
    <div class="row">
        <form name="caminosForm" method="post" class="select-road">
            <div class="form-group row"  style="text-align: center">
                <div class="col-lg-6">
                    <div class="col">
                        <label id="origin-label">Origen</label>
                        <div class="col select-field">
                            <select name='originTown' id='originTown'>
                                <option disabled selected>Municipio origen</option>
                                <?php
                                foreach ($rowArucas as $value) {
                                    echo "<option value=" . $value['nombre'] . ">" . $value['nombre'] . "</option>";
                                }
                                foreach ($rowFirgas as $value) {
                                    echo "<option value=" . $value['nombre'] . ">" . $value['nombre'] . "</option>";
                                }
                                ?>
                            </select>  
                        </div>
                        <div class="col select-field">
                            <select name='origen' id='origen'>
                                <option  disabled selected>Lugar origen</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="col">
                        <label id="desti-label">Destino</label>
                        <div class="col select-field">
                            <select name='destinationTown' id='destinationTown'>
                                <option disabled selected>Municipio destino</option>
                                <?php
                                foreach ($rowArucas as $value) {
                                    echo "<option value=" . $value['nombre'] . ">" . $value['nombre'] . "</option>";
                                }
                                foreach ($rowFirgas as $value) {
                                    echo "<option value=" . $value['nombre'] . ">" . $value['nombre'] . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col select-field">
                            <select name='destino' id='destino'>    
                                <option selected disabled>Lugar destino</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>    
            <div style="text-align: center">
                <input id="button-form" class="btn btn-primary" type="submit" onclick="selectorFunction(event)" value="Crear sendero">
            </div>
        </form>
    </div>
</div>