<!doctype html>
<html lang="en">
    <head>
        <?php
        include 'header-link.php';
        include './modal.php';
        ?>
        <title>Senderos</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        @import url(ol-cesium/node_modules/@camptocamp/cesium/Build/CesiumUnminified/Widgets/CesiumWidget/CesiumWidget.css);
    </style>
    <body>
        <div class="container">

            <?php include 'header.php' ?>

            <div class="main-wrapper">
                <div>
                    <h2 class="title-pages">Búsqueda de senderos</h2>
                </div>
                <div>
                    <h4 class="title-section">Mediante formulario</h4>
                </div>
                <?php
                include './db_functions.php';
                $connection = db_conection();

                $sql = "SELECT id,nombre from municipio where nombre='ARUCAS'";
                $resultArucas = pg_query($sql);
                $rowArucas = pg_fetch_all($resultArucas);

                $sql = "SELECT id,nombre from municipio where nombre='FIRGAS'";
                $resultFirgas = pg_query($sql);
                $rowFirgas = pg_fetch_all($resultFirgas);

                $sql = "SELECT name FROM puntos_arucas order by id";
                $result = pg_query($sql);
                $row = pg_fetch_all($result);
                ?>

                <?php
                include './form_track.php';
                ?>
                <hr id="hr-separator">
                <div>
                    <h4 class="title-section">Interactivo</h4>
                    <div id="interactive-order">
                        <p id="p1">1. Clicke en el mapa para definir un punto de origen.<br>
                            2. Clicke en el mapa para definir un punto de destino.</p>
                        <a  href="#seccion-altimetria" class="float"><img id="ctl00_XXX" src="img/alt-icon.png" width="40px" height="40px"/>
                        </a>
                    </div>
                </div>
            </div>
            <div class="main-wrapper">
                <div id="map" class="map">
                </div>
                <div id="radioVeloContainer">
                    <h4 class="title-section"><i class="fas fa-tachometer-alt"></i></h4>
                    <form>
                        <div class="colum-radio speed-group">
                            <div class="custom-control custom-radio">
                                <input type="radio" name="speed" value="600"> 
                                <label class="label-radio">Lento</label>
                            </div>

                            <div class="custom-control custom-radio">

                                <input id="radio1Vel" type="radio" name="speed" value="300" checked>
                                <label class="label-radio">Normal</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" name="speed" value="50">
                                <label class="label-radio">Rápido</label>
                            </div>

                        </div>
                    </form>
                </div>
                <div id="radiocontainer">
                    <h4 class="title-section"><i class="fas fa-plane"></i></h4>
                    <div id="radio-inline">
                        <form>
                            <div class="colum-radio">
                                <div class="custom-control custom-radio">
                                    <input id="radio0" type="radio" name="optradio" value="0" checked> 
                                    <label class="label-radio">&nbsp;0%</label>
                                </div>

                                <div class="custom-control custom-radio">
                                    <input type="radio" name="optradio" value="10">
                                    <label class="label-radio">10%</label>
                                </div>


                                <div class="custom-control custom-radio">
                                    <input type="radio" name="optradio" value="20">
                                    <label class="label-radio">20%</label>
                                </div>

                                <div class="custom-control custom-radio">
                                    <input type="radio" name="optradio" value="30">
                                    <label class="label-radio">30%</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" name="optradio" value="40">
                                    <label class="label-radio">40%</label>
                                </div>
                            </div>
                            <div class="colum-radio">
                                <div class="custom-control custom-radio">
                                    <input type="radio" name="optradio" value="50">
                                    <label class="label-radio">50%</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" name="optradio" value="60">
                                    <label class="label-radio">60%</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" name="optradio" value="70">
                                    <label class="label-radio">70%</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" name="optradio" value="80">
                                    <label class="label-radio">80%</label>
                                </div >
                                <div class="custom-control custom-radio">
                                    <input type="radio" name="optradio" value="90">
                                    <label class="label-radio">90%</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="check-container">
                    <h4 class="title-section"><i class="fas fa-video"></i></h4>
                    <form>
                        <div class="form-check-inline">
                            <label class="checkbox-inline"><input id="check-follow" type="checkbox" name="followCamera">Seguir</label>
                        </div>
                    </form>
                </div>
                <div id="cesiumContainer"></div>
            </div>
            <div class="main-wrapper">
                <div id="seccion-altimetria">
                    <svg id="profile-graph" class="graph"></svg>
                </div>
            </div>
            <?php
            modal::modalEquals();
            modal::modalNotComplete();
            modal::modalSpinner();
            ?>
            <div id="miModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Contenido del modal -->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Aviso</h5>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>No se han encontrado senderos para los puntos seleccionados.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            db_close_connection($connection);
            include 'footer.php';
            ?>
        </div>
    </div>
</body>
<script>
    $(document).ready(function () {
        initMap();
    });



    var form = document.getElementById('caminosForm');
    function selectorFunction(event) {
        event.preventDefault();

        if (!caminosForm.checkValidity()) {
            return false;
        }
        var ajax = new XMLHttpRequest();
        ajax.open("POST", "query/caminosForm.php", false);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var var1 = document.getElementById("origen").value;
        var var2 = document.getElementById("destino").value;
        ajax.send("origen=" + var1 + "&destino=" + var2);

        if (var1 === var2) {
            $(document).ready(function () {
                $("#modalEquals").modal("show");
            });
        } else {
            var parts = ajax.responseText.split('|');
            var geojson = parts[0];
            console.log(geojson);
            var start = parts[1];
            var end = parts[2];
            if (start !== undefined && end !== undefined) {
                $(document).ready(function () {
                    map.addControl(followControl);
                });
                addSelectorPoints(geojson, start, end);
            } else {
                $(document).ready(function () {
                    $("#modalNotComplete").modal("show");
                });
            }
        }
    }

    $(document).ready(function () {
        $("#originTown").change(function () { 
            var town = $(this).val();

           
            $.get("query/pointTownOrigin.php?town=" + town,
                    function (data) {
                        $("#origen").html(data);
                    });
        });

        $("#destinationTown").change(function () { 
            var town = $(this).val(); 

            $.get("query/pointTownDest.php?town=" + town,
                    function (data) {
                        $("#destino").html(data);
                    });
        });


    });

</script>

</html>