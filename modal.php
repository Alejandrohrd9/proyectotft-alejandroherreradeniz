<?php

class modal {

    public static function modalEquals() {
        echo '<div id = "modalEquals" class = "modal fade" role = "dialog">
        <div class = "modal-dialog modal-dialog-centered">
        <!--Contenido del modal -->
        <div class = "modal-content">
        <div class = "modal-header">
        <h5 class = "modal-title">Aviso</h5>
        <button type = "button" class = "close" data-dismiss = "modal">&times;
        </button>
        </div>
        <div class = "modal-body">
        <p>El punto origen y destino son iguales. Seleccione puntos diferentes.</p>
        </div>
        <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                            </div>
        </div>
        </div>
        </div>';
    }

    public static function modalNotComplete() {
        echo '<div id = "modalNotComplete" class = "modal fade" role = "dialog">
        <div class = "modal-dialog modal-dialog-centered">
        <!--Contenido del modal -->
        <div class = "modal-content">
        <div class = "modal-header">
        <h5 class = "modal-title">Aviso</h5>
        <button type = "button" class = "close" data-dismiss = "modal">&times;
        </button>
        </div>
        <div class = "modal-body">
        <p>Complete todos los campos del formulario.</p>
        </div>
        <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                            </div>
        </div>
        </div>
        </div>';
    }

    public static function modalSpinner() {
        echo '<div class="modal fade modal-spinner" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-body">
        <div class="d-flex align-items-center">
        <strong>Cargando animación...</strong>
        <div class="spinner-border ml-auto" role="status" aria-hidden="true"></div>
        </div>
        </div>
        </div>
        </div>
        </div>';
    }

}
